package enums;

public enum TestEnvironment {
    DEFAULT,
    PROD,
    PREP,
    UAT
}