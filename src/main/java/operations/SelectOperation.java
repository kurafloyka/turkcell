package operations;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

public class SelectOperation {

    private WaitOperation waitOperation;
    private Logger log = Logger.getLogger(SelectOperation.class);
    private String logMessage = "";

    public SelectOperation(WebDriver driver) {
        waitOperation = new WaitOperation(driver);
    }

    public void chooseItemOnTheSelectElementByIndex(By by, int index) {

        try {
            waitOperation.waitPresence(by);
            waitOperation.waitVisible(by);
            waitOperation.waitUntilReadyForDocumentObjectModel();
            WebElement webElement =  waitOperation.waitClickable(by);
            Select select = new Select(webElement);
            select.selectByIndex(index);
        }
        catch (StaleElementReferenceException | NoSuchElementException exception) {
            logMessage = String.format("'%s' select objesinde '%s' indeks'i seçerken sorun oluştu!", by, index);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            chooseItemOnTheSelectElementByIndex(by, index);
        }
    }

    public void chooseItemOnTheSelectElementByValue(By by, String value) {

        waitOperation.waitPresence(by);
        waitOperation.waitVisible(by);
        waitOperation.waitUntilReadyForDocumentObjectModel();
        WebElement webElement =  waitOperation.waitClickable(by);
        Select select = new Select(webElement);
        select.selectByValue(value);
    }

    public void chooseItemOnTheSelectElementByVisibleText(By by, String value) {

        try {
            waitOperation.waitPresence(by);
            waitOperation.waitVisible(by);
            waitOperation.waitUntilReadyForDocumentObjectModel();
            WebElement webElement = waitOperation.waitClickable(by);
            Select select = new Select(webElement);
            select.selectByVisibleText(value);
        }
        catch (StaleElementReferenceException | NoSuchElementException exception) {
            logMessage = String.format("'%s' select objesinde '%s' text değerini seçerken sorun oluştu!", by, value);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            chooseItemOnTheSelectElementByVisibleText(by, value);
        }
    }
}

