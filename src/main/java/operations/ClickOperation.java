package operations;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

public class ClickOperation {

    private WebDriver webDriver;
    private WaitOperation waitOperation;
    private ScrollOperation scrollOperation;

    private Logger log = Logger.getLogger(ClickOperation.class);
    private String logMessage = "";

    public ClickOperation(WebDriver driver) {

        waitOperation = new WaitOperation(driver);
        webDriver = driver;
        scrollOperation = new ScrollOperation(driver);
    }

    public void click(By by) {

        try {
            waitOperation.waitPresence(by);
            waitOperation.waitVisible(by);
            scrollOperation.scrollToElement(by);
            WebElement webElement = waitOperation.waitClickable(by);
            webElement.click();

        }
        catch (StaleElementReferenceException | ElementClickInterceptedException exception) {
            logMessage = String.format("'%s' elementine tıklanırken sorun oluştu!", by);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            click(by);
        }
    }

    public void clickWithJavaScript(By by) {

        try {
            waitOperation.waitPresence(by);
            waitOperation.waitVisible(by);
            scrollOperation.scrollToElement(by);
            WebElement webElement = waitOperation.waitClickable(by);
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
            javascriptExecutor.executeScript("arguments[0].click();", webElement);

        }
        catch (StaleElementReferenceException | ElementClickInterceptedException exception) {
            logMessage = String.format("'%s' elementine tıklanırken sorun oluştu!", by);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            clickWithJavaScript(by);
        }
    }

    public void clickIfExists(By locator, int waitSecond) {

        boolean elementIsExist = new WaitOperation(webDriver).isPresence(locator, waitSecond);
        if (elementIsExist) {
            clickWithJavaScript(locator);
        }
    }

    public void hover(By by) {

        Actions action = new Actions(webDriver);
        WebElement webElement = waitOperation.waitPresence(by);
        action.moveToElement(webElement).build().perform();
    }

    public void doubleClick(By by) {

        try {
            Actions action = new Actions(webDriver);
            waitOperation.waitPresence(by);
            waitOperation.waitVisible(by);
            scrollOperation.scrollToElement(by);
            WebElement webElement = waitOperation.waitClickable(by);
            action.doubleClick(webElement).perform();

        }
        catch (StaleElementReferenceException | ElementClickInterceptedException exception) {
            logMessage = String.format("'%s' elementine çift tıklanırken sorun oluştu!", by);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            doubleClick(by);
        }
    }
}