package operations;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class WaitOperation {

    private WebDriver driver;

    private static int totalStaticWait = 0;
    private int defaultTimeoutInSecond = 30;
    private int defaultIntervalMilliSecond = 1000;

    private static Logger log = Logger.getLogger(WaitOperation.class);

    public WaitOperation(WebDriver webDriver) {

        this.driver = webDriver;

    }

    public WebElement waitPresence(By by) {

        return waitPresence(by, defaultTimeoutInSecond);
    }

    public WebElement waitPresence(By by, int second) {

        WebElement webElement = null;
        try {
            waitUntilReadyForDocumentObjectModel();
            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            webElement = webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementi sayfa üzerinde var olması beklenirken sorun oluştu! Hata kodu: %s", by, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
        return webElement;
    }

    public WebElement waitVisible(By by) {

        return waitVisible(by, defaultTimeoutInSecond);
    }

    public WebElement waitVisible(By by, int second) {

        WebElement webElement = null;
        try {
            waitUntilReadyForDocumentObjectModel();

            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            webElement = webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementi sayfa üzerinde görünür olması beklenirken sorun oluştu! Hata kodu: %s", by, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
        return webElement;
    }

    public void waitInvisible(By by) {

        waitInvisible(by, defaultTimeoutInSecond);
    }

    public void waitInvisible(By locator, int second) {
        try {
            waitUntilReadyForDocumentObjectModel();

            new WebDriverWait(driver, second, defaultIntervalMilliSecond)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementi sayfa üzerinde kaybolması beklenirken sorun oluştu! Hata kodu: %s", locator, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
    }

    public WebElement waitClickable(By by) {

        return waitClickable(by, defaultTimeoutInSecond);
    }

    public WebElement waitClickable(By by, int second) {

        WebElement webElement = null;
        try {
            waitUntilReadyForDocumentObjectModel();
            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            webElement = webDriverWait.until(ExpectedConditions.elementToBeClickable(by));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementi sayfa üzerinde tıklanabilir olması beklenirken sorun oluştu! Hata kodu: %s", by, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
        return webElement;
    }

    public boolean isPresence(By by, int second) {

        WebElement webElement = null;
        try {
            waitUntilReadyForDocumentObjectModel();
            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            webElement = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        }
        catch (Exception ex) {
            String warningMessage = String.format("'%s' elementi '%s' saniye boyunca DOM'a yüklenmesi beklendi fakat yüklenmedi", by, second);
            log.warn(warningMessage);
        }
        return webElement != null;
    }

    public void waitUntilReadyForDocumentObjectModel() {
        new WebDriverWait(driver, defaultTimeoutInSecond).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    public static void wait(int second) {
        try {
            Thread.sleep(second * (long) 1000);
            totalStaticWait += second;
            String logMessage = String.format("'%s' saniye STATIK bekleme yapıldı!", second);
            log.warn(logMessage);
        }
        catch (Exception ex) {
            log.error("STATIK bekleme yapılırken hata oluştu. Hata kodu: " + ex.getMessage());
        }
    }

    /**
     * Sadece Exception'lar için kullanılıyor. Exception harici static beklemeler için lütfen 'wait()' metodunu kullanın.
     */
    public static void waitDoNotWriteToLogFile(int second) {
        try {
            Thread.sleep(second * (long) 1000);
        }
        catch (Exception ex) {
            log.error("STATIK bekleme yapılırken hata oluştu. Hata kodu: " + ex.getMessage());
        }
    }

    public static void writeTotalStaticTime() {

        if(totalStaticWait <= 0)
            return;

        String warningMessage = String.format("Toplam: '%s' saniye STATIK bekleme yapıldı! Lütfen STATIK bekleme sürelerinizi azaltın!", totalStaticWait);
        log.warn(warningMessage);
    }

    public List<WebElement> waitPresenceOfAllElements(By by, int second) {

        List<WebElement> elementList = new ArrayList<>();
        try {

            waitUntilReadyForDocumentObjectModel();
            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            elementList = webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementleri sayfa üzerinde görünür olması beklenirken sorun oluştu! Hata kodu: %s", by, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
        return elementList;
    }

    public void isObjectNotClickable(By by) {
        isObjectNotClickable(by, defaultTimeoutInSecond);
    }

    public void isObjectNotClickable(By by, int second) {

        try {
            waitUntilReadyForDocumentObjectModel();
            WebDriverWait webDriverWait = new WebDriverWait(driver, second, defaultIntervalMilliSecond);
            webDriverWait.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(by)));
        }
        catch (Exception ex) {
            String errorMessage = String.format("'%s' elementi sayfa üzerinde tıklanabilir olmaması beklenirken sorun oluştu! Hata kodu: %s", by, ex.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
    }


}

