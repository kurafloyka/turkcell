package util;

import enums.BrowserType;
import enums.TestEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.concurrent.TimeUnit;

public class WebDriverConfiguration {

    private WebDriver webDriver;
    private BrowserType browserType;
    private TestEnvironment testEnvironment;
    private ChromeOptions chromeOptions;
    private FirefoxOptions firefoxOptions;
    private InternetExplorerOptions internetExplorerOptions;


    public WebDriverConfiguration(WebDriver webDriver, BrowserType browserType, TestEnvironment testEnvironment) {
        this.webDriver = webDriver;
        this.browserType = browserType;
        this.testEnvironment = testEnvironment;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public TestEnvironment getTestEnvironment() {
        return testEnvironment;
    }

    public void setTestEnvironment(TestEnvironment testEnvironment) {
        this.testEnvironment = testEnvironment;
    }

    public void setOptions(BrowserType browserType) {

        switch (browserType) {

            case CHROME:
                chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--test-type");
                chromeOptions.addArguments("--disable-popup-blocking");
                chromeOptions.addArguments("--disable-headless");
                chromeOptions.addArguments("--disable-notifications");
                chromeOptions.addArguments("--start-maximized");
                chromeOptions.addArguments("--lang=tr");



                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");


                webDriver = new ChromeDriver(chromeOptions);
                webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
                webDriver.manage().window().maximize();

                switch (testEnvironment) {
                    case PROD:
                        webDriver.get("https://www.turkcell.com.tr");
                        break;
                    case DEFAULT:
                        webDriver.get("https://default:www.turkcell.com.tr");
                        break;
                    case UAT:
                        webDriver.get("https://UAT:www.turkcell.com.tr");
                        break;
                    case PREP:
                        webDriver.get("https://PREP:www.turkcell.com.tr");
                        break;
//
                }
                break;

            case IE:
                System.out.println("IE icin ayarlama yapilabilir....");
                break;
            case FIREFOX:
                System.out.println("FIREFOX icin ayarlama yapilabilir.");
                break;
        }


    }

    public void closeBrowser() {

        getWebDriver().close();
        getWebDriver().quit();
    }
}
