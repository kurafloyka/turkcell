import enums.BrowserType;
import enums.TestEnvironment;
import operations.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import util.WebDriverConfiguration;


public class BaseTest {
    public Logger log = Logger.getLogger(BaseTest.class);
    public WebDriver webDriver;
    public WebDriverConfiguration webDriverConfiguration;


    public ClickOperation clickOperation;
    public WaitOperation waitOperation;
    public SendKeysOperation sendKeyOperation;
    public SelectOperation selectOperation;
    public ScrollOperation scrollOperation;
    public AssertionOperation assertionOperation;


    @Before
    public void runBeforeTestMethod() {


        PropertyConfigurator.configure("src/main/resources/log4j.properties");

        TestEnvironment testEnvironment = TestEnvironment.PROD;
        BrowserType browserType = BrowserType.CHROME;


        webDriverConfiguration = new WebDriverConfiguration(webDriver, browserType, testEnvironment);
        webDriverConfiguration.setOptions(webDriverConfiguration.getBrowserType());
        clickOperation = new ClickOperation(webDriverConfiguration.getWebDriver());
        waitOperation = new WaitOperation(webDriverConfiguration.getWebDriver());
        sendKeyOperation = new SendKeysOperation(webDriverConfiguration.getWebDriver());
        selectOperation = new SelectOperation(webDriverConfiguration.getWebDriver());
        scrollOperation = new ScrollOperation(webDriverConfiguration.getWebDriver());
        assertionOperation = new AssertionOperation();

    }


    @After
    public void runAfterTestMethod() {

        //webDriverConfiguration.closeBrowser();

        log.info("******************** %s SENARYOSU BITTI ********************");
    }
}
