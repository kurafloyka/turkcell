import org.junit.Test;
import org.openqa.selenium.*;

import java.util.HashMap;
import java.util.List;


public class e2eScenario extends BaseTest {


    @Test
    public void addSmartPhone() {

        //Step2
        clickOperation.hover(setLocator("Cihazlar"));
        clickOperation.clickWithJavaScript(setLocator("Akilli Telefonlar"));


        //Step3
        //Sayfadaki Listelenen telefonlarin bilgisi listede tutuluyor.
        List<WebElement> listOfTel = webDriverConfiguration.getWebDriver().findElements(setLocator("Akilli Cihaz Tablosu"));

        log.info(listOfTel.size());


        for (int i = 0; i < listOfTel.size(); i++) {


            log.info(listOfTel.get(i).getAttribute("data-product-id"));

            webDriverConfiguration.getWebDriver().
                    get("https://www.turkcell.com.tr/cihazlar/akilli-telefonlar/"
                            + listOfTel.get(i).getAttribute("data-product-id"));

            if (webDriverConfiguration.getWebDriver().findElement(setLocator("Sepete Ekle")).isDisplayed()) {

                //Listede sepete ekle gorunur ise tikla ve cik
                log.info(i + ". indeksli telefon stokta ve satisa hazir.");
                break;

            }
        }

        //Step4
        clickOperation.click(setLocator("Pesin Fiyat"));
        clickOperation.click(setLocator("Sepete Ekle"));


        //Step5
        clickOperation.clickWithJavaScript(setLocator("Turkcelli Degilim Butonu"));

        //Step6
        clickOperation.clickWithJavaScript(setLocator("Kullanici Sozlesmesi"));
        clickOperation.clickWithJavaScript(setLocator("Kullanici Sozlesme Onaylama"));
        clickOperation.clickWithJavaScript(setLocator("Siparise Devam Et Butonu"));

        //Step7
        sendKeyOperation.sendKeys(setLocator("Kullanici Adi Soyadi"), "FARUK AKYOL");
        sendKeyOperation.sendKeys(setLocator("Kullanici GSM"), "455430080");
        sendKeyOperation.sendKeys(setLocator("Kullanici Email"), "farukakyol1@icloud.com");
        sendKeyOperation.sendKeys(setLocator("TCKN"), "11111111110");


        sendKeyOperation.sendKeys(setLocator("Teslim Alan Adi Soyadi"), "AHMET AKYOL");
        sendKeyOperation.sendKeys(setLocator("Teslim Alan GSM"), "0554639034");

        clickOperation.click(setLocator("Teslim Alan Il"));
        sendKeyOperation.sendKeys(setLocator("Teslim Alan Il Secimi"), "ADANA");
        clickOperation.click(setLocator("Teslim Alan Il Secilen"));
        sendKeyOperation.sendKeys(setLocator("Teslim Alan Adresi"), "TURKCELL URUN ADRESI");
        clickOperation.clickWithJavaScript(setLocator("Siparise Devam Et Butonu 2"));


        //Step8

        sendKeyOperation.sendKeys(setLocator("Kredi Karti Sahibi"), "FARUK AKYOL");
        sendKeyOperation.sendKeys(setLocator("Kredi Karti Numarasi"), "1234123412341234");
        clickOperation.click(setLocator("Kredi Karti Ay"));
        clickOperation.click(setLocator("Kredi Karti Ay Numarasi"));
        clickOperation.click(setLocator("Kredi Kari Yil"));
        clickOperation.click(setLocator("Kredi Karti Yil Numarasi"));
        sendKeyOperation.sendKeys(setLocator("Kredi Karti CCV"), "245");
        clickOperation.clickWithJavaScript(setLocator("Siparise Devam Et Butonu"));


        //Step9
        clickOperation.clickWithJavaScript(setLocator("Kredi Karti Hatali Format Uyarisi"));


    }

    // Locators tutuldugu liste
    public static By setLocator(String key) {
        HashMap<String, By> locators = new HashMap();


        locators.put("Cihazlar", By.xpath("//nav/a[text()='Cihazlar' and @title='Cihazlar']"));
        locators.put("Akilli Telefonlar", By.xpath("//li/a[text()='Akıllı Telefonlar']"));
        locators.put("Akilli Cihaz Tablosu", By.xpath("//div/a[starts-with(@href, '/cihazlar/akilli-telefonlar/')]"));
        locators.put("Sepete Ekle", By.xpath("//button[text()='Sepete Ekle']"));
        locators.put("Turkcelli Degilim Butonu", By.xpath("//a/span[contains(text(),'Turkcell')]"));
        locators.put("Pesin Fiyat", By.xpath("//div[@class='a-price-radio-b price-radio-cash']"));
        locators.put("Kullanici Sozlesmesi", By.xpath("//span//b[contains(text(),'Kullanıcı sözleşmesini ')]"));
        locators.put("Kullanici Sozlesme Onaylama", By.xpath("//a[contains(text(),'Onayla')]"));
        locators.put("Siparise Devam Et Butonu", By.xpath("//button[contains(text(),'Siparişe Devam Et')]"));

        locators.put("Kullanici Adi Soyadi", By.id("fullName"));
        locators.put("Kullanici GSM", By.id("gsm-1"));
        locators.put("TCKN", By.id("tckimlik"));
        locators.put("Kullanici Email", By.id("email"));

        locators.put("Teslim Alan Adi Soyadi", By.id("customerDeliveryAddressName"));
        locators.put("Teslim Alan GSM", By.id("customerDeliveryGsm"));

        locators.put("Teslim Alan Il", By.xpath("(//div[@class='m-form-group__child'])[7]"));
        locators.put("Teslim Alan Il Secimi", By.xpath("//input[@class='select2-search__field']"));
        locators.put("Teslim Alan Il Secilen", By.xpath("//li[contains(text(),'Adana') and @title='Adana']"));

        locators.put("Teslim Alan Adresi", By.id("textarea-01"));
        locators.put("Siparise Devam Et Butonu 2", By.xpath("//button[contains(text(),'Siparişe Devam Et')]"));

        locators.put("Kredi Karti Sahibi", By.id("ccCardHolderName"));
        locators.put("Kredi Karti Numarasi", By.id("ccCardNumber"));
        locators.put("Kredi Karti Ay", By.id("select2-ccMonth-container"));
        locators.put("Kredi Karti Ay Numarasi", By.xpath("//ul[@id='select2-ccMonth-results']/li[1]"));
        locators.put("Kredi Kari Yil", By.id("select2-ccYear-container"));
        locators.put("Kredi Karti Yil Numarasi", By.xpath("//ul[@id='select2-ccYear-results']/li[4]"));
        locators.put("Kredi Karti CCV", By.id("ccSecurityNumber"));
        locators.put("Kredi Karti Hatali Format Uyarisi", By.xpath("//a[contains(text(),'Kapat')]"));


        for (String i : locators.keySet()) {
            //System.out.println(i + " : " + locators.get(i));
            if (key.equals(i)) {
                return locators.get(i);
            }
        }

        return null;
    }


}
